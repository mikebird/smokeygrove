package main

import (
	"github.com/gin-gonic/gin"
	booking "gitlab.com/mikebird/smokeygrove/internal"
)

func main() {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	r.GET("/booking", booking.GetAllBookings)
	r.Run()
}
